#define MAX_NAME_LENGTH 80
#define MSG_LEN 120

typedef struct ind_struct{
    unsigned int pid;
    int type;
    long int gene;
    char name[MAX_NAME_LENGTH];
}individual, *individual_ptr;

typedef struct pop_struct{
    int readCount;
    int writeCount;
    int individial_count;
    int data_offset;
} population, *population_ptr;

struct msgbuf {
	long mtype;             /* message type, must be > 0 */
	individual mind;    /* message data */
};

void lock_write(int);
void unlock_write(int);
void lock_read(population_ptr, int);
void unlock_read(population_ptr, int);
