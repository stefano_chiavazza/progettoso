#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <sys/msg.h>
#include <sys/ipc.h>
#include "population.h"

void signal_handler(int);
void set_signalhandler();
void unmask_signal();

volatile sig_atomic_t signalStatus = 0;

int main(int argc, char * argv[], char *envp[]){
    int i = 0;
    struct sigaction action;
    sigset_t my_mask, mask, old;
    int msg_id;
    struct msgbuf request;
    individual b_ind;
    //printf("pid=%i, type=%c, file=%s\n", getpid(),'A', argv[0]);
    //printf("pid=%i, type=%c, gene=%s\n", getpid(),'A', argv[1]);
    //printf("pid=%i, type=%c, name=%s\n", getpid(),'A', argv[2]);



    set_signalhandler();
    unmask_signal();
    sleep(5);
    if(!signalStatus){
        pause();
    }
    msg_id = msgget(getpid(), 0600 | IPC_CREAT);
    msgrcv(msg_id, &request, sizeof(individual), 1, 0);
    b_ind = request.mind;
    printf("[%i] request from: pid=%i, type=%i, gene=%li, name=%s\n", getpid(), b_ind.pid, b_ind.type, b_ind.gene, b_ind.name);
    msgctl(msg_id, IPC_RMID, NULL);


}

void set_signalhandler(){
    struct sigaction action;
    sigset_t my_mask;

    action.sa_handler = &signal_handler;
    action.sa_flags = 0;
    sigemptyset(&my_mask);
    action.sa_mask = my_mask;
    sigaction(SIGUSR1, &action, NULL);
}

void unmask_signal(){
    sigset_t mask, old;

    sigaddset(&mask, SIGUSR1);
    sigprocmask(SIG_UNBLOCK, &mask, &old);
}

void signal_handler(int signum){
    if(signum == SIGUSR1){
        signalStatus = 1;
    }
}
