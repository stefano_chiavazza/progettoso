#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <sys/sem.h>
#include "population.h"

void lock_write(int sem_id){
    struct sembuf sem_ctl[2];
    sem_ctl[0].sem_num = 2;//try reading
    sem_ctl[0].sem_op = -1;
    sem_ctl[0].sem_flg = 0;

    sem_ctl[1].sem_num = 3;//resource
    sem_ctl[1].sem_op = -1;
    sem_ctl[1].sem_flg = 0;

    semop(sem_id, sem_ctl, 2);
}

void unlock_write(int sem_id){
    struct sembuf sem_ctl[2];
    sem_ctl[0].sem_num = 2;//try reading
    sem_ctl[0].sem_op = 1;
    sem_ctl[0].sem_flg = 0;

    sem_ctl[1].sem_num = 3;//resource
    sem_ctl[1].sem_op = 1;
    sem_ctl[1].sem_flg = 0;

    semop(sem_id, sem_ctl, 2);
}

void lock_read(population_ptr pop_ptr, int population_sem_id){
    /*
    0 = read mutext
    1 = write mutex
    2 = try reading
    3 = resource
    */
    struct sembuf sem_ctl[2];
    sem_ctl[0].sem_num = 2;//try reading
    sem_ctl[0].sem_op = -1;
    sem_ctl[0].sem_flg = 0;

    sem_ctl[1].sem_num = 0;//readcout mutex
    sem_ctl[1].sem_op = -1;
    sem_ctl[1].sem_flg = 0;

    semop(population_sem_id, sem_ctl, 2);
    pop_ptr->readCount ++;
    if(pop_ptr->readCount == 1){
        sem_ctl[0].sem_num = 3;
        sem_ctl[0].sem_op = -1;
        sem_ctl[0].sem_flg = 0;

        semop(population_sem_id, sem_ctl, 1);
    }

    sem_ctl[0].sem_num = 2;//try reading
    sem_ctl[0].sem_op = 1;
    sem_ctl[0].sem_flg = 0;

    sem_ctl[1].sem_num = 0;//readcout mutex
    sem_ctl[1].sem_op = 1;
    sem_ctl[1].sem_flg = 0;

    semop(population_sem_id, sem_ctl, 2);

}

void unlock_read(population_ptr pop_ptr, int population_sem_id){
    struct sembuf sem_ctl[2];
    sem_ctl[0].sem_num = 0;//readcout mutex
    sem_ctl[0].sem_op = -1;
    sem_ctl[0].sem_flg = 0;

    semop(population_sem_id, sem_ctl, 1);
    pop_ptr->readCount --;

    if(pop_ptr->readCount == 0){
        sem_ctl[0].sem_num = 3;
        sem_ctl[0].sem_op = 1;
        sem_ctl[0].sem_flg = 0;

        semop(population_sem_id, sem_ctl, 1);
    }

    sem_ctl[0].sem_num = 0;//readcout mutex
    sem_ctl[0].sem_op = 1;
    sem_ctl[0].sem_flg = 0;

    semop(population_sem_id, sem_ctl, 1);

}
