#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <sys/wait.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <sys/sem.h>
#include <sys/msg.h>
#include <signal.h>
#include "population.h"

#define GENE_BUFFER_STR 32
#define COMMAND_COMPILE_A "gcc A.c population.c -o A"
#define COMMAND_COMPILE_B "gcc B.c population.c -o B"
#define NUM_SEM_POPULATION 4

int init_people = 5, birth_death=100, sim_time=1000;
long int genes = 5;

int population_shm_id, population_sem_id;

void * create_shm(int *, size_t);
void remove_shm(int, void *);
int create_population(individual_ptr);
void generate_individual(individual_ptr);//generate random characteristic
void get_argv(char *argv[], individual_ptr);//generate args for child
void set_signalhandler();

int main(int argc, char * argv[]){
    int status, i;
    population_ptr my_pop_ptr;

    system(COMMAND_COMPILE_A);
    system(COMMAND_COMPILE_B);

    set_signalhandler();

    my_pop_ptr = create_shm(&population_shm_id, sizeof(population) + sizeof(individual) * init_people);
    my_pop_ptr->readCount = 0;
    my_pop_ptr->writeCount = 0;
    my_pop_ptr->individial_count = init_people;
    my_pop_ptr->data_offset= sizeof(population) + 1;//calculate the pointer after the population struct

    //creation of the semaphore for the control of the population shm
    population_sem_id = semget(IPC_PRIVATE, NUM_SEM_POPULATION, 0600);
    for(i=0; i<NUM_SEM_POPULATION; i++){
        semctl(population_sem_id, i, SETVAL, 1);
    }
    /*
    0 = read mutext
    1 = write mutex
    2 = try reading
    3 = resource
    */
    lock_write(population_sem_id);//contained in population.c
    if(!create_population((individual_ptr)(my_pop_ptr + my_pop_ptr->data_offset))){
        //error
        exit(-1);
    }
    unlock_write(population_sem_id);//contained in population.c
    while(wait(&status) > 0){

    }

    //sleep(30);

    for(i=0; i<init_people; i++){
        msgctl((((individual_ptr)my_pop_ptr + my_pop_ptr->data_offset)+i)->pid,IPC_RMID, NULL);
    }

    remove_shm(population_shm_id, my_pop_ptr);
    semctl(population_sem_id, 0, IPC_RMID);
}

void * create_shm(int * shm_id, size_t size){
    void * ptr;
    * shm_id = shmget(IPC_PRIVATE, size, 0600);
    ptr = shmat(* shm_id, NULL, 0);
    return ptr;
}

void remove_shm(int shm_id, void * ptr){
    shmdt(ptr);
    shmctl(shm_id, IPC_RMID, NULL);
}

int create_population(individual_ptr ind_list){
    int i, pid;

    sigset_t mask;
    sigset_t old;

    char sem_str[32];
    char shm_str[32];
    sprintf(shm_str, "SHM_ID=%i", population_shm_id);
    sprintf(sem_str, "SEM_ID=%i", population_sem_id);

    char *argv[4];
    char *envp[3] = {shm_str, sem_str, NULL};

    //srand(time(NULL));//initialize random seed
    srand(0);
    for(i=0; i<init_people; i++){

        //individual new_ind = * (pop_list + i);
        individual_ptr new_individual_ptr = ind_list + i;

        generate_individual(new_individual_ptr);
        get_argv(argv, new_individual_ptr);//generate the array of arguments

        pid = fork();
        switch(pid){
            case 0://child
                sigaddset(&mask, SIGUSR1);
                sigprocmask(SIG_BLOCK, &mask, &old);
                execve(argv[0], argv, envp);
                fprintf(stderr, "%s\n", strerror(errno));
                exit(-1);
            case -1://error
                return 0;
            default://parent
                printf("gestore type=%i, gene=%li, name=%s, pid=%i\n", new_individual_ptr->type, new_individual_ptr->gene, new_individual_ptr->name, pid);
                new_individual_ptr->pid = pid;
                free(argv[1]);
                break;
        }
    }
    return 1;
}

void generate_individual(individual_ptr ind){
    ind->type = rand() % 2;
    ind->gene = rand() % (genes -2) + 2;
    ind->name[0] = (char) ((rand() % 25) + 65);
    ind->name[1] = 0;
}

void get_argv(char *argv[], individual_ptr ind){
    char * temp = malloc(GENE_BUFFER_STR * sizeof(char));
    if(ind->type==1){
        argv[0] = "A";
    }else {
        argv[0] = "B";
    }
    sprintf(temp, "%li", ind->gene);
    argv[1] = temp;
    argv[2] = ind->name;
    argv[3]=NULL;
}

void signal_handler(int signum){
    if(signum == SIGINT){
        shmctl(population_shm_id, IPC_RMID, NULL);
        semctl(population_sem_id, 0, IPC_RMID);
    }
}
void set_signalhandler(){
    struct sigaction action;
    sigset_t my_mask;

    action.sa_handler = &signal_handler;
    action.sa_flags = 0;
    sigemptyset(&my_mask);
    action.sa_mask = my_mask;
    sigaction(SIGINT, &action, NULL);
}
