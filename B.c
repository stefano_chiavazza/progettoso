#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <sys/wait.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <sys/sem.h>
#include <signal.h>
#include <sys/msg.h>
#include <string.h>
#include "population.h"


#define SHM_INDEX 0
#define SEM_INDEX 1
#define MAX_NAME_LENGTH 80

long int gcd(long int, long int);
individual_ptr find_best_partner(population_ptr, long int);

int main(int argc, char * argv[], char * envp[]){
    int i = 0, shm_id, sem_id, msg_id;
    long int my_gene, max_gcd;
    char * my_name;
    population_ptr pop_ptr;
    individual_ptr best_partner_ptr;
    individual my_ind;

    struct msgbuf request;

    sscanf(argv[1], "%li", &my_gene);

    my_ind.pid = getpid();
    my_ind.type = 0;
    my_ind.gene = my_gene;
    strcpy(my_ind.name, argv[2]);

    //printf("pid=%i, type=%i, gene=%li, name=%s\n", my_ind.pid, my_ind.type, my_ind.gene, my_ind.name);

    sscanf(envp[SHM_INDEX], "SHM_ID=%i", &shm_id);
    sscanf(envp[SEM_INDEX], "SEM_ID=%i", &sem_id);

    pop_ptr = shmat(shm_id, NULL, 0);

    lock_read(pop_ptr, sem_id);//contained in population.c
    best_partner_ptr = find_best_partner(pop_ptr, my_gene);
    unlock_read(pop_ptr, sem_id);//contained in population.c

    printf("[%i] best partner: pid=%i, gene=%li, name=%s\n",getpid(),  best_partner_ptr->pid, best_partner_ptr->gene, best_partner_ptr->name);

    msg_id = msgget(best_partner_ptr->pid, 0600 | IPC_CREAT);
    request.mtype = 1;
    request.mind = my_ind;

    msgsnd(msg_id, &request, sizeof(individual), 0);

    kill(best_partner_ptr->pid, SIGUSR1);
}

individual_ptr find_best_partner(population_ptr pop_ptr, long int my_gene){
    int max_gcd, current_gcd, i;
    individual_ptr best_partner_ptr, ind_ptr;

    best_partner_ptr = (individual_ptr)(pop_ptr + pop_ptr->data_offset);
    max_gcd = gcd(best_partner_ptr->gene, my_gene);

    for(i=0; i<pop_ptr->individial_count; i++){
        ind_ptr = ((individual_ptr)(pop_ptr + pop_ptr->data_offset)) + i;
        current_gcd = gcd(ind_ptr->gene, my_gene);
        if(ind_ptr->type == 1 && current_gcd > max_gcd){
            max_gcd = current_gcd;
            best_partner_ptr = ind_ptr;
        }
    }
    return best_partner_ptr;
}

long int gcd(long int a, long int b){//greatest common divisor
    if(a == 0 || b == 0){
        return 0;
    }
    if(a == b){
        return a;
    }
    if(a > b){
        return gcd(a-b, b);
    }
    return gcd(a, b-a);
}
